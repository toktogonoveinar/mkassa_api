package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.requests.ApiRequest;
import io.restassured.response.Response;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static com.Mkassa_API.API.mkassaApi.EndPoints.*;
import static com.Mkassa_API.API.utils.GenerateRandomDate.generateRandomDate;

public class WithdrawController extends ApiRequest {
    public WithdrawController(String url) {
        super(url, BASE_HEADERS);
    }

    public Response getWithdrawLast(){
        this.response = super.get(getEndPoint(API,WITHDRAW,LAST_WITHDRAW));
        return response;
    }

    public Response getWithdrawForWeek(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("days", "7");
        parameter.put("limit", "999");
        this.response = super.get(getEndPoint(API,WITHDRAW,V2_STATEMENT),parameter);
        return response;
    }

    public Response getWithdrawForMonths(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("days", "30");
        parameter.put("limit", "999");
        this.response = super.get(getEndPoint(API,WITHDRAW,V2_STATEMENT),parameter);
        return response;
    }

    public Response getWithdrawForPeriod(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString() );
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        this.response = super.get(getEndPoint(API,WITHDRAW,V2_STATEMENT),parameter);
        return response;
    }


}
