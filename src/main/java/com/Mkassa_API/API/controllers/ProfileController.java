package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.requests.ApiRequest;
import io.restassured.response.Response;

import java.util.HashMap;

import static com.Mkassa_API.API.mkassaApi.EndPoints.*;

public class ProfileController extends ApiRequest {

    public ProfileController(String url) {
        super(url, BASE_HEADERS);
    }

    public Response getCashiers(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "200");
        this.response = super.get(getEndPoint(API,V1,CASHIERS),parameter);
        return response;
    }

    public Response getLocality(){
        this.response = super.get(getEndPoint(API,LOCALITY));
        return response;
    }

    public Response getBranches(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "100");
        this.response = super.get(getEndPoint(API,V1,BRANCHES),parameter);
        return response;
    }

    public Response getSupervisors(){
        this.response = super.get(getEndPoint(API,USERS,V1,SUPERVISORS));
        return response;
    }

    public Response getMainPage(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("is_fiscalized_only", "true");
        parameter.put("include_cash", "true");
        this.response = super.get(getEndPoint(API,TRANSACTION,MAIN_PAGE), parameter);
        return  response;
    }

    public Response getOperationsSettings(){
        this.response = super.get(getEndPoint(API,V1,COMPANIES,GET_OPERATIONS_SETTINGS));
        return  response;
    }
}
