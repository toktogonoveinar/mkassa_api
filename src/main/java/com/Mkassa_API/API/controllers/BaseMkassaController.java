package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.config.ConfigReader;
import lombok.Data;
import lombok.Getter;

@Getter
@Data
public class BaseMkassaController {


    private final UsersController usersController;
    private final TransactionController transactionController;
    private final ProfileController profileController;
    private final WithdrawController withdrawController;
    private final CompanyBranchController companyBranchController;
    private final KkmController kkmController;
    public BaseMkassaController (){
        this.usersController = new UsersController(ConfigReader.getProperty("url"));
        this.transactionController = new TransactionController(ConfigReader.getProperty("url"));
        this.profileController = new ProfileController(ConfigReader.getProperty("url"));
        this.withdrawController = new WithdrawController(ConfigReader.getProperty("url"));
        this.companyBranchController = new CompanyBranchController(ConfigReader.getProperty("url"));
        this.kkmController = new KkmController(ConfigReader.getProperty("url"));
    }
}
