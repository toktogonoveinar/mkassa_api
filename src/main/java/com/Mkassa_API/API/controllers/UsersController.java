package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.models.users.*;
import com.Mkassa_API.API.requests.ApiRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;

import static com.Mkassa_API.API.mkassaApi.EndPoints.*;
import static com.Mkassa_API.API.models.users.CheckCodeRequest.getCheckCode;
import static com.Mkassa_API.API.models.users.SendCodeRequest.getSendCode;
import static io.restassured.RestAssured.given;


@Slf4j
public class UsersController extends ApiRequest {


    public UsersController(String url) {
        super(url,BASE_HEADERS);
    }

    public static String postSendCode1() {
        RestAssured.baseURI = "https://api.mkassa.kg";
        SendCode sendCode = getSendCode();
        String code = given()
                .contentType("application/json")
                .body(sendCode)
                .when()
                .post("/api/users/send_code/")
                .then()
                .statusCode(201)
                .extract().response()
                .jsonPath().getString("code");

        return code;
    }

    public static void main(String[] args) {
        postSendCode1();
    }

    public static Response postSendCode(){
        RestAssured.baseURI = "https://api.mkassa.kg";
        CheckCode checkOtp = getCheckCode(postSendCode1());
        Response otp = given()
                .contentType("application/json")
                .body(checkOtp)
                .when()
                .post("/api/users/check_code/")
                .then()
                .extract().response();

        return otp;

    }

    public static   String getBearerToken1() {
        return postSendCode().asPrettyString();

    }




    public Response postArmLogin(Login login){
       return this.response = super.post(getEndPoint(API,ARM,LOGIN)
               ,login.toJson(login));
    }

    public Response  postSendCode(SendCode sendCode){
        return this.response = super.post(getEndPoint(API,USERS,SEND_CODE)
                ,sendCode.toJson(sendCode));
    }
    public Response getUserMe(){
        return this.response = super.get(getEndPoint(API,USERS,ME));
    }

    public Response postUserLogin(UserLogin userLogin){
        return this.response = super.post(getEndPoint(API,USERS,LOGIN)
                ,userLogin.toJson(userLogin));

    }
    public Response getUserRkoAccountInfo(){
        return this.response = super.get(getEndPoint(API,USERS,RKO_ACCOUNT_INFO));
    }

    public Response getUserCanTransactions(){
        return this.response = super.get(getEndPoint(API,USERS,CAN_TRANSACTIONS));
    }

    public Response postUserMBankCheck(UserMBankCheck userMBankCheck){
        return this.response = super.post(getEndPoint(API,USERS,CHECK_MBANK)
                ,userMBankCheck.toJson(userMBankCheck));
    }

    public Response getCheckWork(){
        return this.response = super.get(getEndPoint(API,USERS,CHECK_WORK));
    }

    public Response getFactoringMe(){
        return this.response = super.get(getEndPoint(API,USERS,FACTORING_ME));
    }

    public Response postMposLogin(MposLogin mposLogin){
        return this.response = super.post(getEndPoint(API,USERS,MPOS_LOGIN)
                , mposLogin.toJson(mposLogin));

    }

    public Response postCheckVersion(CheckVersion checkVersion){
        return this.response = super.post(getEndPoint(API,USERS,V1,CHECK_VERSION)
                ,checkVersion.toJson(checkVersion));
    }

}
