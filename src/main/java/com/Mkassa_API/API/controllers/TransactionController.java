package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.models.transactions.TransactionsDebug;
import com.Mkassa_API.API.requests.ApiRequest;
import io.restassured.response.Response;

import java.time.LocalDate;
import java.util.HashMap;


import static com.Mkassa_API.API.mkassaApi.EndPoints.*;
import static com.Mkassa_API.API.utils.GenerateRandomDate.generateRandomDate;

public class TransactionController extends ApiRequest {
    public TransactionController(String url) {
        super(url, BASE_HEADERS);
    }

    public Response getTransaction(){
        return this.response = super.get(getEndPoint(API,TRANSACTION));
    }

    public Response postTransactionsDebug(TransactionsDebug transactionsDebug){
        return this.response = super.post(getEndPoint(API,TRANSACTIONS_DEBUG)
                ,transactionsDebug.toJson(transactionsDebug));
    }

    public Response getTransactions(){
        return this.response = super.get(getEndPoint(API,V2,TRANSACTIONS));
    }

    public Response getTransactionsForWeek(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "7");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForMonths(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "30");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionPeriod(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString());
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }


    public Response getTransactionsTypeQr(){
        HashMap<String,String> parameter = new HashMap<>();
        parameter.put("limit","999");
        parameter.put("start_date","2024-01-23");
        parameter.put("end_date","2024-01-23");
        parameter.put("type","qr");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForWeekTypeQr(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "7");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","qr");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForMonthsTypeQr(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "30");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","qr");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionPeriodTypeQr(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString());
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsTypeCard(){
        HashMap<String,String> parameter = new HashMap<>();
        parameter.put("limit","999");
        parameter.put("start_date","2024-01-23");
        parameter.put("end_date","2024-01-23");
        parameter.put("type","card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForWeekTypeCard(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "7");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionsForMonthsTypeCard(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "30");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionPeriodTypeCard(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString());
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionsTypeQrStatusPaid(){
        HashMap<String,String> parameter = new HashMap<>();
        parameter.put("limit","999");
        parameter.put("start_date","2024-01-23");
        parameter.put("end_date","2024-01-23");
        parameter.put("type","qr");
        parameter.put("status", "paid");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForWeekTypeQrStatusPaid(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "7");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","qr");
        parameter.put("status", "paid");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForMonthsTypeQrStatusPaid() {
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "30");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        parameter.put("status", "paid");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionPeriodTypeQrStatusPaid(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString());
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        parameter.put("status", "paid");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionsTypeQrStatusRollBackCard(){
        HashMap<String,String> parameter = new HashMap<>();
        parameter.put("limit","999");
        parameter.put("start_date","2024-01-23");
        parameter.put("end_date","2024-01-23");
        parameter.put("type","qr");
        parameter.put("status", "rollback_card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForWeekTypeQrStatusRollBackCard(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "7");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","qr");
        parameter.put("status", "rollback_card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForMonthsTypeQrStatusRollBackCard() {
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "30");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        parameter.put("status", "rollback_card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionPeriodTypeQrStatusRollBackCard(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString());
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        parameter.put("status", "rollback_card");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }


    public Response getTransactionsTypeQrStatusFailed(){
        HashMap<String,String> parameter = new HashMap<>();
        parameter.put("limit","999");
        parameter.put("start_date","2024-01-23");
        parameter.put("end_date","2024-01-23");
        parameter.put("type","qr");
        parameter.put("status", "failed");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForWeekTypeQrStatusFailed(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "7");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type","qr");
        parameter.put("status", "failed");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS),parameter);
        return response;
    }

    public Response getTransactionsForMonthsTypeQrStatusFailed() {
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "30");
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        parameter.put("status", "failed");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }

    public Response getTransactionPeriodTypeQrStatusFailed(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        LocalDate randomStartDate = generateRandomDate();
        parameter.put("start_date", randomStartDate.toString());
        LocalDate randomEndDate = generateRandomDate();
        parameter.put("end_date", randomEndDate.toString());
        parameter.put("branch", "2819");
        parameter.put("include_cash", "true");
        parameter.put("type", "qr");
        parameter.put("status", "failed");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS), parameter);
        return response;
    }




}
