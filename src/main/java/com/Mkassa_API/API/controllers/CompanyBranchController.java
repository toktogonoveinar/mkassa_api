package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.requests.ApiRequest;
import io.restassured.response.Response;

import java.util.Map;

import static com.Mkassa_API.API.mkassaApi.EndPoints.*;

public class CompanyBranchController extends ApiRequest {
    public CompanyBranchController(String url) {
        super(url, BASE_HEADERS);
    }

    public Response getShiftBranches(){
        this.response = super.get(getEndPoint(API,COMPANY_BRANCH,SHIFT_BRANCHES));
        return response;
    }

    public Response getMyBranches(){
        this.response = super.get(getEndPoint(API,COMPANY_BRANCH,MY_BRANCHES));
        return response;
    }
}
