package com.Mkassa_API.API.controllers;

import com.Mkassa_API.API.requests.ApiRequest;
import io.restassured.response.Response;


import java.util.HashMap;

import static com.Mkassa_API.API.mkassaApi.EndPoints.*;

public class KkmController extends ApiRequest {
    public KkmController(String url) {
        super(url, BASE_HEADERS);
    }

    public Response getCheckUser(){
        return this.response = super.get(getEndPoint(API,KKM,V1,CHECK_USER));
    }
    public Response getMainPageKkm(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("is_fiscalized_only","true");
        parameter.put("include_cash","true");
        this.response = super.get(getEndPoint(API,V2,TRANSACTIONS,MAIN_PAGE),parameter);
        return response;
    }

    public Response getKkmCheckWork(){
        this.response = super.get(getEndPoint(API,USERS,CHECK_WORK));
        return response;
    }

    public Response getCompanyKkm() {
        this.response = super.get(getEndPoint(API,COMPANY,GET_OPERATIONS_SETTINGS));
        return response;
    }

    public Response getSaleReportKkm(){
        HashMap<String, String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        parameter.put("days", "1");
        parameter.put("is_fiscalized_only","true");
        parameter.put("include_cash","true");
        this.response = super.get(getEndPoint(API,V2,KKM,SHIFTS),parameter);
        return response;
    }

    public Response getProductKkm(){
        HashMap<String,String> parameter = new HashMap<>();
        parameter.put("limit", "999");
        this.response = super.get(getEndPoint(API,PRODUCT,PRODUCTS),parameter);
        return response;
    }


}
