package com.Mkassa_API.API.asserts;
import com.Mkassa_API.API.enams.StatusCodes;
import com.Mkassa_API.API.models.users.UserMe;
import io.restassured.response.Response;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@Slf4j
public class ApiAsserts {

    private final Response response;

    public ApiAsserts(Response response){
        this.response = response;
    }

    public static ApiAsserts assertThatResponse(Response response){
        log.info("Start assert response...");
        return new ApiAsserts(response);
    }

   public ApiAsserts isCorrectStatusCode(StatusCodes expectedStatusCode){
       if (this.response == null) assertTrue(false, "Response is null");
       Assertions.assertThat(this.response.getStatusCode())
               .withFailMessage("Response code is not correct Actual %s, Expected %s"
                       ,this.response.getStatusCode(), expectedStatusCode)
               .isEqualTo(expectedStatusCode.getStatus());
       log.info("Status code is correct Actual {}, Expected {}", this.response.getStatusCode(), expectedStatusCode);
       return this;
   }
    public UserAssert assertUser(UserMe user) {
        return UserAssert.assertThat(user);
    }


}
