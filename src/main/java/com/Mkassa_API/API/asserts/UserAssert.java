package com.Mkassa_API.API.asserts;

import com.Mkassa_API.API.models.BaseEntity;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;

@Slf4j
public class UserAssert extends EntityAssert{
    public UserAssert(BaseEntity entity) {
        super(entity);
    }

    public static UserAssert assertThat(BaseEntity entity){
        return new UserAssert(entity);
    }

    public UserAssert isCorrectName(String expectedName){
        Assertions.assertThat(super.entity.receiveName())
                .withFailMessage("User name is not correct, actual {}, expected {}",super.entity.receiveName(),expectedName)
                .isEqualTo(expectedName);
        log.info("Name is correct, Actual {}, Expected {}",super.entity.receiveName(),expectedName);
        return this;
    }

}
