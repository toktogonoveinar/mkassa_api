package com.Mkassa_API.API.requests;

import com.Mkassa_API.API.config.ConfigReader;
import io.restassured.authentication.PreemptiveOAuth2HeaderScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import static io.restassured.RestAssured.given;

@Slf4j
@Data
public class ApiRequest {

    protected String url;
    protected Map<String, String> headers;
    public Response response;
    protected RequestSpecification requestSpecification;

    public ApiRequest(String url, Map<String, String> headers) {
        this.headers = headers;
        this.url = url;
        PreemptiveOAuth2HeaderScheme authScheme = new PreemptiveOAuth2HeaderScheme();
        authScheme.setAccessToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo5NDA5LCJ1c2VybmFtZSI6IjAzNDQwNSIsImV4cCI6MTczNzUyMTUyNCwiZW1haWwiOiJ0b2t0b2dvbm92ZWluYXJAZ21haWwuY29tIiwibG9naW4iOiIwMzQ0MDUiLCJvcmlnX2lhdCI6MTcwNTk4NTUyNH0.eiXP_QkGSVRcnVd7USz1xiNQ8lWz-0cAbP65NaVpJN4");
        requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAuth(authScheme)
                .setBaseUri(this.url)
                .setAccept(ContentType.JSON)
                .addHeaders(this.headers)
                .build();
        requestSpecification.log().all();
    }

    public ApiRequest(String url) {
        this.url = url;
        this.requestSpecification = given()
                .baseUri(this.url)
                .auth()
                .basic(ConfigReader.getProperty("apiKey"), "");
    }

    public static final String SLASH = "/";
    public static String DEFAULT_ENDPOINTS = "";


    public static String getEndPoint(String... args) {
        StringBuilder endPoint = new StringBuilder();
        for (String arg : args) {
            endPoint.append(arg).append(SLASH);
        }
        return endPoint.substring(0, endPoint.length() - 1);
    }

    public static String formatParameters(HashMap<String, String> parameters) {
        if (parameters.isEmpty()) {
            return "";
        }
        StringJoiner query = new StringJoiner("&", "?", "");
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            query.add(entry.getKey() + "=" + entry.getValue());
        }
        return query.toString();
    }

    public ApiRequest logResponse() {
        log.info("Response is \n {}", getResponse().getBody().asPrettyString());
        log.warn("Status code is {}", (getResponse().getStatusCode()));
        return this;
    }

    public Response get(String endPoint) {
        log.info("Performed GET: {}", endPoint);
        this.response = given()
                .spec(requestSpecification)
                .get(endPoint)
                .then().extract().response();
        logResponse();
        return this.response;
    }

    public Response getRequest(String endPoint, Header header) {
        log.info("Performed GET: {}", endPoint);
        log.info("HEADER IS: {}", header);
        this.response = given()
                .spec(requestSpecification)
                .header(header)
                .get(endPoint)
                .then().extract().response();
        logResponse();
        return this.response;
    }

    public Response post(String endPoint, String body) {
        log.info("Performed POST: {}", endPoint);
        log.info("Body is: {}", body);
        this.response = given()
                .spec(requestSpecification)
                .body(body).log().all()
                .post(endPoint);
        logResponse();
        return this.response;
    }

    public Response put(String endPoint, String body) {
        log.info("Performed PUT: {}", endPoint);
        log.info("Body is: {}", body);
        this.response = given()
                .spec(requestSpecification)
                .body(body)
                .put(endPoint);
        logResponse();
        return this.response;
    }

    public Response patch(String endPoint, String body) {
        log.info("Performed PATCH: {}", endPoint);
        log.info("Body is: {}", body);
        this.response = given()
                .spec(requestSpecification)
                .body(body)
                .patch(endPoint);
        logResponse();
        return this.response;
    }

    public Response delete(String endPoint) {
        log.info("Performed DELETE: {}", endPoint);
        this.response = given()
                .spec(requestSpecification)
                .delete(endPoint);
        logResponse();
        return this.response;
    }

    public Response post(String endPoint, Map<String, String> param) {
        log.info("Performed POST: {}", endPoint);
        log.info("FORMAT PARAMS IS: {}", param);
        this.response = given()
                .spec(requestSpecification)
                .formParams(param)
                .post(endPoint);
        logResponse();
        return this.response;
    }

    public Response get(String endPoint, Map<String, String> param){
        log.info("Performed GET: {}", endPoint);
        log.info("FORMAT PARAMS IS: {}", param);
        this.response = given()
                .spec(requestSpecification)
                .formParams(param)
                .get(endPoint);
        logResponse();
        return this.response;
    }

}
