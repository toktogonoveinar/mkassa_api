package com.Mkassa_API.API.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;

@Slf4j
@Data
public class BaseEntity {



    public String toJson(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isEquals(BaseEntity expectedEntity) {
        try {
            Assertions.assertThat(this)
                    .usingRecursiveComparison()
                    .ignoringExpectedNullFields()
                    .isEqualTo(expectedEntity);
            return true;
        } catch (AssertionError e) {
            log.error("Object is not equals \n{}", e.getMessage());
            return false;
        }
    }



    public Object receiveName() {
        return null;
    }
}
