package com.Mkassa_API.API.models.users;

import lombok.Data;

@Data
public class UserLoginRequest {

    public static UserLogin getUserLogin(){
        return UserLogin.builder()
                .deviceUuid("string")
                .notificationToken("string")
                .isIos(false)
                .serialNumber("string")
                .login("328619")
                .password("qweqwe")
                .build();
    }
}
