package com.Mkassa_API.API.models.users;

public class CheckCodeRequest {

    public static CheckCode getCheckCode(String code){
        return CheckCode.builder()
                .code(code)
                .phoneNumber("996500882942")
                .build();
    }
}
