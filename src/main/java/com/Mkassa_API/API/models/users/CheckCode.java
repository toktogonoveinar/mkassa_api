package com.Mkassa_API.API.models.users;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;



@AllArgsConstructor
@Builder
@Data
public class CheckCode {

    private String code;
    @JsonProperty("phone_number")
    private String phoneNumber;
}
