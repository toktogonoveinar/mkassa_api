package com.Mkassa_API.API.models.users;

import com.Mkassa_API.API.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UserFactoringMe extends BaseEntity {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("middle_name")
    private Object middleName;
    @JsonProperty("last_name")
    private String lastName;
    private String login;
    @JsonProperty("rko_account")
    private String rkoAccount;
    @JsonProperty("rko_bic")
    private String rkoBic;
    private String account;
    private String bic;
}
