package com.Mkassa_API.API.models.users;


import com.Mkassa_API.API.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Login extends BaseEntity {

    private String login;
    private String password;
}
