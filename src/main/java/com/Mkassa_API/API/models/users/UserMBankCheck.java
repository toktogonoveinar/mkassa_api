package com.Mkassa_API.API.models.users;

import com.Mkassa_API.API.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UserMBankCheck extends BaseEntity {

    @JsonProperty("phone_number")
    private String phoneNumber;
}
