package com.Mkassa_API.API.models.users;

import lombok.Builder;

@Builder
public class UserMBankCheckRequest {

    public static UserMBankCheck getUserMBank(){
        return UserMBankCheck.builder()
                .phoneNumber("+996500882942")
                .build();
    }
}
