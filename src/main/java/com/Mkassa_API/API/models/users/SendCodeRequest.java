package com.Mkassa_API.API.models.users;



import lombok.Builder;
import lombok.Data;


public class SendCodeRequest {

    public static SendCode getSendCode(){
        return SendCode.builder()
                .phoneNumber("+996500882942")
                .build();
    }
}
