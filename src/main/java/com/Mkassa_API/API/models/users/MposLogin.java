package com.Mkassa_API.API.models.users;

import com.Mkassa_API.API.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class MposLogin extends BaseEntity {

    @JsonProperty("device_uuid")
    private String deviceUuid;
}
