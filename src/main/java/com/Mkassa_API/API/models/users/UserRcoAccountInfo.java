package com.Mkassa_API.API.models.users;


import com.Mkassa_API.API.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UserRcoAccountInfo extends BaseEntity {

    private String bic;
    private String account;
    private int balance;
    private String currency;
}
