package com.Mkassa_API.API.models.users;

import com.Mkassa_API.API.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserMe extends BaseEntity {

   private int id;
   private String login;
   private String inn;
   @JsonProperty("first_name")
   private String firstName;
   @JsonProperty("last_name")
   private String lastName;
   @JsonProperty("user_type")
   private String userType;
   @JsonProperty("middle_name")
   private Object middleName;
   private String email;
   @JsonProperty("phone_number")
   private String phoneNumber;
   private Object gender;
   @JsonProperty("created_date")
   private Date createdDate;
   @JsonProperty("company_id")
   private int companyId;


    public Object receiveId(){
       return getId();
   }

   public Object receiveName(){
        return getFirstName();
   }


}
