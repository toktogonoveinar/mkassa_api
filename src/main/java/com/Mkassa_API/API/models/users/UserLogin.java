package com.Mkassa_API.API.models.users;

import com.Mkassa_API.API.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserLogin extends BaseEntity {

    @JsonProperty("device_uuid")
    private String deviceUuid;
    @JsonProperty("notification_token")
    private String notificationToken;
    @JsonProperty("is_ios")
    private boolean isIos;
    @JsonProperty("serial_number")
    private String serialNumber;
    private String login;
    private String password;
}
