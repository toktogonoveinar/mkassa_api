package com.Mkassa_API.API.models.users;

public class CheckVersionRequest {


    public static CheckVersion getCheckVersion(){
        return CheckVersion.builder()
                .appVersion("2.1.3")
                .build();
    }
}
