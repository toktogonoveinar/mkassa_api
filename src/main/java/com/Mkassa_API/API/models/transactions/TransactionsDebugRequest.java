package com.Mkassa_API.API.models.transactions;

public class TransactionsDebugRequest {

    public static TransactionsDebug getTransactionsDebug(){
        return TransactionsDebug.builder()
                .serialNumber("00023390363")
                .errorMessage("Другая ошибка хоста")
                .transaction("123")
                .build();
    }
}
