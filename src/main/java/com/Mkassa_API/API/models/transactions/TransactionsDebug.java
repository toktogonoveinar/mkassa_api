package com.Mkassa_API.API.models.transactions;


import com.Mkassa_API.API.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class TransactionsDebug extends BaseEntity {

    @JsonProperty("serial_number")
    private String serialNumber;
    @JsonProperty("error_message")
    private String errorMessage;
    private String transaction;
}
