package com.Mkassa.API.transactions;

import com.Mkassa.API.BaseApiTest;
import com.Mkassa_API.API.asserts.ApiAsserts;
import com.Mkassa_API.API.enams.StatusCodes;
import com.Mkassa_API.API.models.transactions.TransactionsDebug;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.Mkassa_API.API.models.transactions.TransactionsDebugRequest.getTransactionsDebug;

public class TransactionTest extends BaseApiTest {

    @BeforeClass
    public void beforeClass(){
        transactionController = baseMkassaController.getTransactionController();
    }

    @Test
    public void transactionsDebug(){
        TransactionsDebug transactionsDebug = getTransactionsDebug();
        transactionController.postTransactionsDebug(transactionsDebug);
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_CREATED);
    }


    @Test(description = "Verify that user can get transaction", priority = 1)
    @Feature("Transaction")
    public void transactionTest() throws InterruptedException {
        try {
            transactionController.getTransaction();
            ApiAsserts.assertThatResponse(transactionController.getResponse())
                    .isCorrectStatusCode(StatusCodes.HTTP_OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        Thread.sleep(2000);

    }

    @Test(description = "Verify that user can get transactions for a day", priority = 2)
    @Feature("Transactions for a day")
    public void transactionsTest() throws InterruptedException {
        transactionController.getTransactions();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }


    @Test(description = "Verify that user can get transactions for a week", priority = 3)
    @Feature("Transactions for a week")
    public void transactionsForWeekTest() throws InterruptedException {
        transactionController.getTransactionsForWeek();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions for a months", priority = 4)
    @Feature("Transactions for a months")
    public void transactionsForMonthsTest() throws InterruptedException {
        transactionController.getTransactionsForMonths();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions for a period", priority = 5)
    @Feature("Transactions for a period")
    public void transactionsForPeriodTest() throws InterruptedException {
        transactionController.getTransactionPeriod();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transaction type QR for a day", priority = 6)
    @Feature("Transactions Type QR Status ALL")
    public void transactionsQrTest() throws InterruptedException {
        transactionController.getTransactionsTypeQr();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR for a week", priority = 7)
    @Feature("Transactions Type QR Status ALL")
    public void transactionsForWeekQrTest() throws InterruptedException {
        transactionController.getTransactionsForWeekTypeQr();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR for a months", priority = 8)
    @Feature("Transactions Type QR Status ALL")
    public void transactionsForMonthsQrTest() throws InterruptedException {
        transactionController.getTransactionsForMonthsTypeQr();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }


    @Test(description = "Verify that user can get transactions type QR for a period", priority = 9)
    @Feature("Transactions Type QR Status All")
    public void transactionsForPeriodQRTest() throws InterruptedException {
        transactionController.getTransactionPeriodTypeQr();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type CARD for a day", priority = 10)
    @Feature("Transactions Type CARD Status All")
    public void transactionsCardTest() throws InterruptedException {
        transactionController.getTransactionsTypeCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type CARD for a week", priority = 11)
    @Feature("Transactions Type CARD Status ALL")
    public void transactionsForWeekCardTest() throws InterruptedException {
        transactionController.getTransactionsForWeekTypeCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that uer can get transactions type CARD for a months", priority = 12)
    @Feature("Transactions Type CARD Status ALL")
    public void transactionsForMonthsCardTest() throws InterruptedException {
        transactionController.getTransactionsForMonthsTypeCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type CARD for a period", priority = 13)
    @Feature("Transactions Type CARD status ALL")
    public void transactionsForPeriodCardTest() throws InterruptedException {
        transactionController.getTransactionPeriodTypeCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR Status paid for a day ", priority = 14)
    @Feature("Transactions Type QR Status PAID")
    public void transactionsQrStatusPaidTest() throws InterruptedException {
        transactionController.getTransactionsTypeQrStatusPaid();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR Status paid for a week", priority = 15)
    @Feature("Transactions Type QR Status PAID")
    public void transactionsForWeekQrStatusPaidTest()throws InterruptedException{
        transactionController.getTransactionsForWeekTypeQrStatusPaid();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR status PAID for a months", priority = 16)
    @Feature("Transactions Type QR Status PAID")
    public void transactionsForMonthsQrStatusPaidTest()throws InterruptedException{
        transactionController.getTransactionsForMonthsTypeQrStatusPaid();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR status paid for a period", priority = 17)
    @Feature("Transactions Type QR Status PAID")
    public void transactionsForPeriodQRStatusPaidTest()throws InterruptedException{
        transactionController.getTransactionPeriodTypeQrStatusPaid();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }


    @Test(description = "Verify that user can get transactions type QR Status RollBack_CARD for a day ", priority = 18)
    @Feature("Transactions Type QR Status RollBack_Card")
    public void transactionsQrStatusRollBackCardTest() throws InterruptedException {
        transactionController.getTransactionsTypeQrStatusRollBackCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR Status RollBack_CARD for a week", priority = 19)
    @Feature("Transactions Type QR Status RollBack_Card")
    public void transactionsForWeekQrStatusRollBackCardTest()throws InterruptedException{
        transactionController.getTransactionsForWeekTypeQrStatusRollBackCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR status RollBack_CARD for a months", priority = 20)
    @Feature("Transactions Type QR Status RollBack_Card")
    public void transactionsForMonthsQrStatusRollBackCardTest()throws InterruptedException{
        transactionController.getTransactionsForMonthsTypeQrStatusRollBackCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR status RollBack_CARD for a period", priority = 21)
    @Feature("Transactions Type QR Status RollBack_Card")
    public void transactionsForPeriodQRStatusRollBackCardTest()throws InterruptedException{
        transactionController.getTransactionPeriodTypeQrStatusRollBackCard();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }


    @Test(description = "Verify that user can get transactions type QR Status Failed for a day ", priority = 22)
    @Feature("Transactions Type QR Status Failed")
    public void transactionsQrStatusFailedTest() throws InterruptedException {
        transactionController.getTransactionsTypeQrStatusFailed();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR Status Failed for a week", priority = 23)
    @Feature("Transactions Type QR Status Failed")
    public void transactionsForWeekQrStatusFailedTest()throws InterruptedException{
        transactionController.getTransactionsForWeekTypeQrStatusFailed();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR status Failed for a months", priority = 24)
    @Feature("Transactions Type QR Status Failed")
    public void transactionsForMonthsQrStatusFailedTest()throws InterruptedException{
        transactionController.getTransactionsForMonthsTypeQrStatusFailed();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get transactions type QR status Failed for a period", priority = 25)
    @Feature("Transactions Type QR Status Failed")
    public void transactionsForPeriodQRStatusFailedTest()throws InterruptedException{
        transactionController.getTransactionPeriodTypeQrStatusFailed();
        ApiAsserts.assertThatResponse(transactionController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }
// 6790
}
