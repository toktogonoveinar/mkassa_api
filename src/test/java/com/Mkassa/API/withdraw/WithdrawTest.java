package com.Mkassa.API.withdraw;

import com.Mkassa.API.BaseApiTest;
import com.Mkassa_API.API.asserts.ApiAsserts;
import com.Mkassa_API.API.enams.StatusCodes;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WithdrawTest extends BaseApiTest  {

    @BeforeClass
    public void beforeClass(){
        withdrawController = baseMkassaController.getWithdrawController();
    }

    @Test(description = "Verify that user can get withdraw for last", priority = 1)
    @Feature("Last Withdraw")
    public void lastWithdrawTest() throws InterruptedException {
        withdrawController.getWithdrawLast();
        ApiAsserts.assertThatResponse(withdrawController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get withdraw for a week", priority = 2)
    @Feature("Withdraw for a week")
    public void withdrawForWeekTest() throws InterruptedException {
        withdrawController.getWithdrawForWeek();
        ApiAsserts.assertThatResponse(withdrawController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get withdraw for a months", priority = 3)
    @Feature("Withdraw for a months")
    public void withdrawForMonths() throws InterruptedException {
        withdrawController.getWithdrawForMonths();
        ApiAsserts.assertThatResponse(withdrawController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get withdraw for a period", priority = 4)
    @Feature("Withdraw for a period")
    public void withdrawForPeriodTest() throws InterruptedException {
        withdrawController.getWithdrawForPeriod();
        ApiAsserts.assertThatResponse(withdrawController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }


}
