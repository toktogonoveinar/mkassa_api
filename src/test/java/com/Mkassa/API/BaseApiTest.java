package com.Mkassa.API;

import com.Mkassa_API.API.controllers.*;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class BaseApiTest {

    protected UsersController usersController;
    protected TransactionController transactionController;
    protected ProfileController profileController;
    protected WithdrawController withdrawController;
    protected CompanyBranchController companyBranchController;
    protected KkmController kkmController;
    protected BaseMkassaController baseMkassaController = new BaseMkassaController();


    @BeforeSuite
    public void beforeSuite(){
        System.out.println("================= Start Api Test ==================");
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("================== End Api Test ==================");
    }
}
