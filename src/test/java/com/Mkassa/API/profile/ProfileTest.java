package com.Mkassa.API.profile;

import com.Mkassa.API.BaseApiTest;
import com.Mkassa_API.API.asserts.ApiAsserts;
import com.Mkassa_API.API.enams.StatusCodes;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ProfileTest extends BaseApiTest {

    @BeforeClass
    public void beforeClass(){
        profileController = baseMkassaController.getProfileController();
    }

    @Test(description = "Verify that user can get cashiers", priority = 1)
    @Feature("Cashiers")
    public void cashiersTest() throws InterruptedException {
        profileController.getCashiers();
        ApiAsserts.assertThatResponse(profileController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get locality", priority = 2)
    @Feature("Locality")
    public void localityTest() throws InterruptedException {
        profileController.getLocality();
        ApiAsserts.assertThatResponse(profileController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get branches", priority = 3)
    @Feature("Branches")
    public void branchesTest() throws InterruptedException {
        profileController.getBranches();
        ApiAsserts.assertThatResponse(profileController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get supervisors", priority = 4)
    @Feature("Supervisors")
    public void supervisorsTest() throws InterruptedException {
        try {
            profileController.getSupervisors();
            ApiAsserts.assertThatResponse(profileController.getResponse())
                    .isCorrectStatusCode(StatusCodes.HTTP_OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        Thread.sleep(2000);

    }

    @Test(description = "Verify that user can get main page data", priority = 5)
    @Feature("Main Page Data")
    public void mainPageDataTest() throws InterruptedException {
        try {
            profileController.getMainPage();
            ApiAsserts.assertThatResponse(profileController.getResponse())
                    .isCorrectStatusCode(StatusCodes.HTTP_OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        Thread.sleep(2000);

    }

    @Test(description = "Verify that user can get Operations Settings", priority = 6)
    @Feature("Operations Settings")
    public void operationsSettingsTest(){
        profileController.getOperationsSettings();
        ApiAsserts.assertThatResponse(profileController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }
}
