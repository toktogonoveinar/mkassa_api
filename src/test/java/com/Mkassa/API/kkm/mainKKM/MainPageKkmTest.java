package com.Mkassa.API.kkm.mainKKM;

import com.Mkassa.API.BaseApiTest;
import com.Mkassa_API.API.asserts.ApiAsserts;
import com.Mkassa_API.API.enams.StatusCodes;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MainPageKkmTest extends BaseApiTest {

    @BeforeClass
    public void beforeClass(){
        kkmController = baseMkassaController.getKkmController();
    }

    @Test
    public void mainPageKkmTest(){
        kkmController.getMainPageKkm();
        ApiAsserts.assertThatResponse(kkmController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }

    @Test
    public void kkmCheckUserTest(){
        kkmController.getCheckUser();
        ApiAsserts.assertThatResponse(kkmController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }

    @Test
    public void kkmCheckWorkTest(){
        kkmController.getKkmCheckWork();
        ApiAsserts.assertThatResponse(kkmController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }

    @Test
    public void kkmCompanyTest(){
        kkmController.getCompanyKkm();
        ApiAsserts.assertThatResponse(kkmController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }


    @Test
    public void kkmSaleReportTest(){
        kkmController.getSaleReportKkm();
        ApiAsserts.assertThatResponse(kkmController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }

    @Test
    public void kkmProductTest(){
        kkmController.getProductKkm();
        ApiAsserts.assertThatResponse(kkmController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }

}
