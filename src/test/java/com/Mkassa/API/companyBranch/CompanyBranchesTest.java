package com.Mkassa.API.companyBranch;

import com.Mkassa.API.BaseApiTest;
import com.Mkassa_API.API.asserts.ApiAsserts;
import com.Mkassa_API.API.enams.StatusCodes;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyBranchesTest extends BaseApiTest {

    @BeforeClass
    public void beforeClass(){
        companyBranchController = baseMkassaController.getCompanyBranchController();
    }

    @Test
    public void shiftBranchesTest(){
        companyBranchController.getShiftBranches();
        ApiAsserts.assertThatResponse(companyBranchController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }

    @Test
    public void myBranchTest(){
        companyBranchController.getMyBranches();
        ApiAsserts.assertThatResponse(companyBranchController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_OK);
    }
}
