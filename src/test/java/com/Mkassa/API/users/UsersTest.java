package com.Mkassa.API.users;

import com.Mkassa.API.BaseApiTest;
import com.Mkassa_API.API.asserts.ApiAsserts;
import com.Mkassa_API.API.enams.StatusCodes;
import com.Mkassa_API.API.models.users.*;
import io.qameta.allure.Feature;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import static com.Mkassa_API.API.enams.StatusCodes.HTTP_OK;
import static com.Mkassa_API.API.models.users.CheckVersionRequest.getCheckVersion;
import static com.Mkassa_API.API.models.users.LoginRequest.getLogin;
import static com.Mkassa_API.API.models.users.MposLoginRequest.getMposLogin;
import static com.Mkassa_API.API.models.users.SendCodeRequest.getSendCode;
import static com.Mkassa_API.API.models.users.UserLoginRequest.getUserLogin;
import static com.Mkassa_API.API.models.users.UserMBankCheckRequest.getUserMBank;



public class UsersTest extends BaseApiTest {

    @BeforeClass
    public void beforeClass(){
        usersController = baseMkassaController.getUsersController();
   }


    @Test(description = "Verify that user can login with valid data", priority = 1)
    @Feature("Sign ARM Login")
    public void armLoginTest() throws InterruptedException {
        Login login = getLogin();
        usersController.postArmLogin(login);
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Assert.assertNotNull(login.getLogin());
        Assert.assertNotNull(login.getPassword());
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can check mpos login with valid device_uuid",priority = 2)
    @Feature("Mpos Login")
    public void mposLoginTest() throws InterruptedException {
        MposLogin mposLogin = getMposLogin();
        usersController.postMposLogin(mposLogin);
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can check version of app mkassa", priority = 3)
    @Feature("Check version App Mkassa")
    public void checkVersionTest() throws InterruptedException {
        CheckVersion checkVersion = getCheckVersion();
        usersController.postCheckVersion(checkVersion);
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Thread.sleep(2000);
    }


    @Test(description = "Verify that user can sign in by phone number", priority = 4)
    @Feature("Send Code")
    public void sendCodeTest() throws InterruptedException {
        SendCode sendCode = getSendCode();
        usersController.postSendCode(sendCode);
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(StatusCodes.HTTP_CREATED);
        Assert.assertNotNull(sendCode.getPhoneNumber());
        Thread.sleep(2000);

    }


    @Test(description = "Verify that user can log in with valid data", priority = 5)
    @Feature("Login")
    public void userLoginTest() throws InterruptedException {
        UserLogin userLogin = getUserLogin();
        usersController.postUserLogin(userLogin);
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Assert.assertNotNull(userLogin.getDeviceUuid());
        Assert.assertFalse(userLogin.isIos());
        Assert.assertEquals(userLogin.getLogin(),"328619");
        Assert.assertEquals(userLogin.getPassword(),"qweqwe");
        Thread.sleep(2000);

    }

    @Test(description = "Verify that user can get all data account", priority = 6)
    @Feature("User Me")
    public void userMeTest() throws InterruptedException {
        usersController.getUserMe();
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Thread.sleep(2000);
    }


    @Test(description = "Verify that user can get RKO info", priority = 7)
    @Feature("RKO Account Info")
    public void userRkoAccountTest() throws InterruptedException {
        usersController.getUserRkoAccountInfo();
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can check can transactions", priority = 8)
    @Feature("Can Transactions")
    public void userCanTransactionsTest() throws InterruptedException {
        usersController.getUserCanTransactions();
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Thread.sleep(2000);

    }

    @Test(description = "Verify that user can check Mbank", priority = 9)
    @Feature("Check Mbank")
    public void userCheckMBankTest() throws InterruptedException {
        UserMBankCheck userMBankCheck = getUserMBank();
        usersController.postUserMBankCheck(userMBankCheck);
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Assert.assertFalse(false);
        Thread.sleep(2000);

    }

    @Test(description = "Verify that user can check work", priority = 10)
    @Feature("Check Work")
    public void userCheckWorkTest() throws InterruptedException {
        usersController.getCheckWork();
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);
        Thread.sleep(2000);
    }

    @Test(description = "Verify that user can get data of factoring me",priority = 11)
    @Feature("Factoring Me")
    public void userFactoringMe(){
        usersController.getFactoringMe();
        ApiAsserts.assertThatResponse(usersController.getResponse())
                .isCorrectStatusCode(HTTP_OK);

    }


}

